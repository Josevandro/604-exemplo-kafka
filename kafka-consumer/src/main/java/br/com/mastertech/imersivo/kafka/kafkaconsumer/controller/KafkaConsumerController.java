package br.com.mastertech.imersivo.kafka.kafkaconsumer.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.mastertech.imersivo.kafka.model.Beirute;

@Component
public class KafkaConsumerController {

	@KafkaListener(topics = "barba-tunado")
	public void recebeBeirute(@Payload Beirute beirute) {
		System.out.println("Recebi um beirute " + beirute.getTamanho());
		System.out.println("Paguei " + beirute.getPreco());
		System.out.println("com sabor de " + beirute.getRecheio());
		System.out.println("");
	}
}
